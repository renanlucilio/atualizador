unit uFTPConfig.interfaces;

interface

type
  IFtp = interface
    ['{A45FBD48-8B47-40CA-A012-4863D9A7C526}']
    function Conectar: IFtp;
    function Baixar(arquivo, Local: string): IFtp;
    function GetTamanhoArquivo(arquivo: string): Integer;
  end;

  ITratarArquivoZIP = interface
    ['{121395AE-48E0-4685-A53B-B54A7BAA2994}']
    function Descompactar(localDescompactacao, ArquivoZIP: string): ITratarArquivoZIP;
  end;

implementation

end.
