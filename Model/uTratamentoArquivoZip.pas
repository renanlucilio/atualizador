unit uTratamentoArquivoZip;

interface

uses
  {interfaces}
    uFTPConfig.interfaces,
  {Delphi}
    System.ZIP,
    System.SysUtils;

type

  TTratamentoArquivoZip = class
  public
    procedure Descompactar(localDescompactacao, ArquivoZIP: string);
  end;

implementation



{ TTratamentoArquivoZip }

procedure TTratamentoArquivoZip.Descompactar(localDescompactacao, ArquivoZIP: string);
var
  zip: TZipFile;
begin
  zip:= TZipFile.Create;
  try
    if FileExists(ArquivoZIP) and zip.IsValid(ArquivoZIP) then
    begin
      if FileExists(localDescompactacao) then
        DeleteFile(localDescompactacao);

      zip.ExtractZipFile(ArquivoZIP, localDescompactacao);
    end;
  finally
    zip.Free;
  end;
end;

end.
