﻿unit uFTPConfig;

interface

uses
  {Indy}
    IdBaseComponent,
    IdComponent,
    IdTCPConnection,
    IdTCPClient,
    IdExplicitTLSClientServerBase,
    IdFTP,
    IdFTPCommon,
    IdException,
  {Interfaces}
    uFTPConfig.interfaces;

type
  TEvBaixando          = procedure(ASender: TObject; AWorkMode: TWorkMode;AWorkCount: Int64) of object;
  TEvComecandoBaixar   = procedure(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64) of object;

  TFTPConfig = class
  private
    FFTP: TIdFTP;

    FServ: string;
    FPort: integer;
    FUser: string;
    FPass: string;
    FOnBaixando: TEvBaixando;
    FOnComecoBaixar: TEvComecandoBaixar;
    FTamanhoDoArquivo: integer;

    procedure ConfigurarFTP;
    procedure CarregarAsInformacoes;
  public
    constructor Create(); overload;
    constructor Create(aUser, aPass, aServ: string; aPort: integer); overload;
    destructor Destroy; override;

    property Serv:                string               read FServ;
    property Port:                integer              read FPort;
    property User:                string               read FUser;
    property Pass:                string               read FPass;
    property TamanhoDoArquivo:    integer              read FTamanhoDoArquivo;
    property OnBaixando:          TEvBaixando          read FOnBaixando          write FOnBaixando;
    property OnComecoBaixar:      TEvComecandoBaixar   read FOnComecoBaixar      write FOnComecoBaixar;

    procedure Conectar;
    procedure Baixar(arquivo, Local: string);
    function GetTamanhoArquivo(arquivo: string): Integer;
  end;

implementation

uses
  {Delphi}
    System.IniFiles,
    System.SysUtils;


{ TFTPConfig }

procedure TFTPConfig.Baixar(arquivo, Local: string);
begin
  FTamanhoDoArquivo:= GetTamanhoArquivo(arquivo);

  if Assigned(OnBaixando) then
    FFTP.OnWork:= OnBaixando;

  if Assigned(OnComecoBaixar) then
  begin
    FFTP.OnWorkBegin:= OnComecoBaixar;
  end;

  try
    if FFTP.Connected then
    begin
      FFTP.Get(
        arquivo,
        Local+arquivo,
        True,
        True
         );
    end;
  except
    on E: Exception do
    begin
      if E is EIdConnClosedGracefully then
        exit;

      raise Exception.Create('Erro ao baixar a atualização'+  e.Message);

      Abort;
    end;
  end;
end;

procedure TFTPConfig.CarregarAsInformacoes;
var
  arquivoIni: string;
  ini: TIniFile;
begin
  arquivoIni:= ExtractFilePath(ParamStr(0));
  arquivoIni:= Concat(arquivoIni, 'att', '.ini');
  ini:= TIniFile.Create(arquivoIni);
  try
    FServ:= ini.ReadString('f', 's', '');
    FPort:= ini.ReadInteger('f', 'p', 0);
    FUser:= ini.ReadString('f', 'u', '');
    FPass:= ini.ReadString('f', 'ps', '');
  finally
    ini.Free;
  end;
end;

procedure TFTPConfig.Conectar;
begin

  if FFTP.Connected then
    FFTP.Disconnect;
  try
    FFTP.Connect;
    FFTP.ChangeDir('atualizacao');
  except
    on E: Exception do
    begin
      raise Exception.Create('Erro ao acessar o servidor de atualização: ' + e.Message);
    end;
  end;
end;

procedure TFTPConfig.ConfigurarFTP;
begin
  FFTP.Host        := Serv;
  FFTP.Port        := Port;
  FFTP.Username    := User;
  FFTP.Password    := Pass;
  FFTP.Passive     := true;
  FFTP.TransferType:= ftBinary;
end;

constructor TFTPConfig.Create(aUser, aPass, aServ: string; aPort: integer);
begin
  FUser:= aUser;
  FPass:= aPass;
  FServ:= aServ;
  FPort:= aPort;

  FFTP:= TIdFTP.Create(nil);
  ConfigurarFTP;
end;

constructor TFTPConfig.Create;
begin
  CarregarAsInformacoes;
  if Serv.IsEmpty or User.IsEmpty or Pass.IsEmpty or (Port = 0) then
    raise Exception.Create('Configurações FTP inválidas');

  FFTP:= TIdFTP.Create(nil);
  ConfigurarFTP;
end;

destructor TFTPConfig.Destroy;
begin
  FFTP.Disconnect;
  FFTP.Free;
  inherited;
end;

function TFTPConfig.GetTamanhoArquivo(arquivo: string): Integer;
begin
  if FFTP.Connected then
  begin
    result:= FFTP.Size(arquivo);
  end
  else
    result:= 0;
end;

end.
