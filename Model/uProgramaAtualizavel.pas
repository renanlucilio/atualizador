unit uProgramaAtualizavel;

interface

uses
  {$IFDEF FMX}
    FMX.Forms,
  {$ELSE}
    VCL.Forms,
  {$ENDIF}
   uFTPConfig;

type

  TProgramaAtualizavel = class
  private
    FFTP:             TFTPConfig;
    FNomePrograma:    string;
    FVersaoLocal:     integer;
    FLocalPrograma:   string;
    FArquivoIniFTP:   string;
    FVersaoFTP:       integer;

    procedure Descompactar(arquivoZIP: string);
    function GetVersaoLocal(alocalPrograma: string):integer;
    function GetVersaoFTP: integer;
    function carregarConfiguracoes(): string;
  public
    constructor Create(aFtp: TFTPConfig);
    destructor Destroy; override;

    property FTP:           TFTPConfig           read FFTP;
    property NomePrograma:  string               read FNomePrograma;
    property ArquivoIniFTP: string               read FArquivoIniFTP;
    property LocalPrograma: string               read FLocalPrograma;
    property VersaoLocal:   integer              read FVersaoLocal;
    property VersaoFTP:     integer              read FVersaoFTP;

    procedure Atualizar;
  end;

implementation

uses
  {TratarArquivo}
    uTratamentoArquivoZip,
  {Delphi}
    System.SysUtils,
    System.IniFiles,
    Winapi.Windows,
    ShellApi,
  {Util}
    Model.LibUtil;

{ TProgramaAtualizavel }

procedure TProgramaAtualizavel.Atualizar;
var
  nomeZip: string;
  ProgramaBKP: string;
  localFTP: string;
  localIniAtt: string;
  exe: string;
begin
  exe:= Concat(LocalPrograma, NomePrograma, '.exe');
  localIniAtt:= ExtractFilePath(ParamStr(0))+'att.ini';
  localFTP:= ExtractFilePath(Concat(LocalPrograma,ArquivoIniFTP));
  nomeZip:= Concat(LocalPrograma, NomePrograma, '.zip');
  if FileExists(nomeZip) then
    DeleteFile(PWideChar(nomeZip));

  if VersaoLocal < VersaoFTP then
  begin
    FFTP.Conectar;
    FFTP.Baixar(NomePrograma+'.zip', LocalPrograma);

    ProgramaBKP:=Concat(LocalPrograma, NomePrograma, '_bkp.exe');

    if FileExists(ProgramaBKP) then
      DeleteFile(PWideChar(ProgramaBKP));

    RenameFile(exe, ProgramaBKP);

    Descompactar(nomeZip);

    if FileExists(localFTP) then
      DeleteFile(PWideChar(localFTP));

    if FileExists(localIniAtt) then
      DeleteFile(PWideChar(localIniAtt));

    ShellExecute(0, 'open', PWideChar(exe), nil, nil, SW_SHOWNORMAL);
  end
  else
  begin
    Application.Terminate;
  end;
end;

function TProgramaAtualizavel.carregarConfiguracoes: string;
var
  arquivoIni: string;
  ini: TIniFile;
begin
  arquivoIni:= ExtractFilePath(ParamStr(0));
  arquivoIni:= Concat(arquivoIni, 'att', '.ini');
  ini:= TIniFile.Create(arquivoIni);
  try
    Result:= ini.ReadString('f', 'pr', '');
  finally
    ini.Free;
  end;
end;


constructor TProgramaAtualizavel.Create(aFtp: TFTPConfig);
var
  tempNomePrograma: string;
  alocalPrograma: string;
begin
  alocalPrograma:= carregarConfiguracoes;

  if alocalPrograma.IsEmpty then
    raise Exception.Create('Nome Programa inv�lido');

  tempNomePrograma:=ExtractFileName(alocalPrograma);
  tempNomePrograma:= ChangeFileExt(tempNomePrograma, '');

  FFTP           := aFtp;
  FNomePrograma  := tempNomePrograma;
  FLocalPrograma := ExtractFilePath(alocalPrograma);
  FVersaoLocal   := GetVersaoLocal(alocalPrograma);
  FArquivoIniFTP := 'versaoFTP.ini';
  FVersaoFTP     := GetVersaoFTP();
end;

procedure TProgramaAtualizavel.Descompactar(arquivoZIP: string);
var
  tratarArquivo: TTratamentoArquivoZip;
begin
  tratarArquivo:= TTratamentoArquivoZip.Create();
  try
    tratarArquivo.Descompactar(LocalPrograma, arquivoZIP);
  finally
    tratarArquivo.Free;
  end;
end;

destructor TProgramaAtualizavel.Destroy;
begin
  if Assigned(FFTP) then
    FFTP.Free;

  inherited;
end;

function TProgramaAtualizavel.GetVersaoFTP: integer;
var
  strVersao: string;
  arquivoINI: TIniFile;
  localINIFTP: string;
begin
  localINIFTP:= LocalPrograma+ArquivoIniFTP;
  Result:= 0;
  if FileExists(localINIFTP) then
    DeleteFile(PWideChar(localINIFTP));

  if Assigned(FTP) then
  begin
    FTP.Conectar;
    FTP.Baixar(ArquivoIniFTP, LocalPrograma);

    if FileExists(localINIFTP) then
    begin
      arquivoINI:= TIniFile.Create(LocalPrograma+ArquivoIniFTP);
      try
        strVersao:= arquivoINI.ReadString('versao', NomePrograma, EmptyStr);

        strVersao:= StringReplace(strVersao, '.', EmptyStr, [rfReplaceAll]);

        Result:= StrToIntDef(strVersao, 0);
      finally
        arquivoINI.Free;
      end;
    end;
  end;
end;

function TProgramaAtualizavel.GetVersaoLocal(alocalPrograma: string): integer;
var
  strVersao:string;
begin
  if FileExists(alocalPrograma) then
  begin
    strVersao:= GetVersaoPrograma(alocalPrograma);
    strVersao:= StringReplace(strVersao, '.', EmptyStr, [rfReplaceAll]);
    Result:= StrToIntDef(strVersao, 0);
  end
  else
    Result:= 0;
end;

end.
