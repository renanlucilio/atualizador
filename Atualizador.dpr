program Atualizador;

uses
  Vcl.Forms,
  TratandoErro,
  Winapi.Windows,
  uViewPrincipal in 'View\uViewPrincipal.pas' {frmPrincipal},
  uFTPConfig in 'Model\uFTPConfig.pas',
  uProgramaAtualizavel in 'Model\uProgramaAtualizavel.pas',
  uTratamentoArquivoZip in 'Model\uTratamentoArquivoZip.pas',
  uVM.FTP in 'ViewModel\uVM.FTP.pas' {dmFTP: TDataModule};

{$R *.res}

var
  programa: THandle;
  titulo: string;
begin
  titulo:= 'Atualizador SICLOP';
  programa := FindWindow(nil, PWideChar(titulo));
  if programa = 0 then
  begin
    {$IFDEF DEBUG}
      ReportMemoryLeaksOnShutdown:= true;
    {$ELSE}
      ReportMemoryLeaksOnShutdown:= false;
    {$ENDIF}


    Application.Title:=titulo;
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(TdmFTP, dmFTP);
  Application.CreateForm(TfrmPrincipal, frmPrincipal);
  Application.Run;
  end
  else
    SetForegroundWindow(programa);
end.
