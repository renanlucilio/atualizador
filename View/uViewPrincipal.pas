unit uViewPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls,
  Vcl.ExtCtrls;

type
  TfrmPrincipal = class(TForm)
    pgbProgresso: TProgressBar;
    lblProcesso: TLabel;
    lblPorcentagem: TLabel;
    Label1: TLabel;
    tmrInicia: TTimer;
    procedure tmrIniciaTimer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.dfm}

uses uVM.FTP;

procedure TfrmPrincipal.FormShow(Sender: TObject);
begin
  tmrInicia.Enabled:= true;
end;

procedure TfrmPrincipal.tmrIniciaTimer(Sender: TObject);
begin
  tmrInicia.Enabled:= false;
  dmFTP.iniciar;

end;

end.
