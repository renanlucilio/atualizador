object frmPrincipal: TfrmPrincipal
  Left = 0
  Top = 0
  BorderIcons = [biMinimize, biMaximize]
  BorderStyle = bsNone
  Caption = 'Atualizador SICLOP'
  ClientHeight = 121
  ClientWidth = 470
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblProcesso: TLabel
    Left = 9
    Top = 21
    Width = 105
    Height = 13
    AutoSize = False
  end
  object lblPorcentagem: TLabel
    Left = 8
    Top = 71
    Width = 449
    Height = 27
    Alignment = taCenter
    AutoSize = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 0
    Top = 0
    Width = 470
    Height = 19
    Align = alTop
    Alignment = taCenter
    Caption = 'Atualizador SICLOP'
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    ExplicitWidth = 139
  end
  object pgbProgresso: TProgressBar
    Left = 8
    Top = 40
    Width = 449
    Height = 25
    TabOrder = 0
  end
  object tmrInicia: TTimer
    Enabled = False
    Interval = 2500
    OnTimer = tmrIniciaTimer
    Left = 224
    Top = 48
  end
end
