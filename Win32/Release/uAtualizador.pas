unit uAtualizador;

interface

uses
  {Delphi}
    System.Classes,
    System.IniFiles,
    System.SysUtils,
  {$IFDEF FMX}
    FMX.Forms
  {$ELSE}
    VCL.Forms
  {$ENDIF};

type
  TAtualizador = class
  private
    FVersaoLocal: integer;
    FVersaoFTP:   integer;
    FUser:        string;
    FPass:        string;
    FServ:        string;
    FPort:        integer;
    function Desatualizado: Boolean;
    procedure AbrirAtualizador;
    function GetVersaoFTP: integer;
    procedure GetArquivoIniFTP;
    procedure GetAtualizador;
    procedure limparArquivo();
  public
    constructor Create();
    destructor Destroy; override;

    property VersaoLocal: integer read FVersaoLocal;
    property VersaoFTP:   integer read FVersaoFTP;
  end;

var
  atualiazador: TAtualizador;

implementation

uses
  {Utils}
    Model.LibUtil,
  {FTP}
    uFTPConfig,
  {Delphi}
    System.Types,
    ShellApi,
    Winapi.Windows,
  {Power}
    Model.PowerCMD;

{ TAtualizador }

function TAtualizador.Desatualizado: Boolean;
var
  versaoLocalstr: string;
begin
  versaoLocalstr:= GetVersaoPrograma(ParamStr(0));
  versaoLocalstr:= StringReplace(versaoLocalstr, '.', '', [rfReplaceAll]);

  result:= StrToIntDef(versaoLocalstr, 0) < GetVersaoFTP;
end;

procedure TAtualizador.AbrirAtualizador;
var
  atualizador: string;
  iniAtualizador:TIniFile;
begin
  atualizador:= ExtractFilePath(ParamStr(0))+'att.exe';

  if Desatualizado then
  begin
    GetArquivoIniFTP;
    GetAtualizador;
    iniAtualizador:= TIniFile.Create(ExtractFilePath(ParamStr(0))+'att.ini');
    try
      iniAtualizador.WriteString('f', 'pr', ParamStr(0));

      ShellExecute(HInstance, 'open', PWideChar(atualizador), '', '', SW_SHOWNORMAL);
    finally
      iniAtualizador.Free;
      Application.Terminate;
    end;
  end
  else
  begin
    if FileExists(ExtractFilePath(ParamStr(0))+'att.ini') then
      DeleteFile(PWideChar(ExtractFilePath(ParamStr(0))+ 'att.ini'));

    if FileExists(ExtractFilePath(ParamStr(0))+'att.exe') then
      DeleteFile(PWideChar(ExtractFilePath(ParamStr(0))+ 'att.exe'));
  end;
end;

constructor TAtualizador.Create();
var
  iniAtualizador: TIniFile;
  localIni, localAtt: string;
begin
  limparArquivo();
  localIni:= ExtractFilePath(ParamStr(0))+'att.ini';
  localAtt:= ExtractFilePath(ParamStr(0))+'att.exe';

  try
    GetArquivoIniFTP;
  except
    exit;
  end;

  iniAtualizador:= TIniFile.Create(localIni);
  try
    FUser:= iniAtualizador.ReadString('f', 'u', '');
    FPass:= iniAtualizador.ReadString('f', 'ps', '');
    FServ:= iniAtualizador.ReadString('f', 's', '');
    FPort:= iniAtualizador.ReadInteger('f', 'p', 0);
  finally
    iniAtualizador.Free;
  end;
  AbrirAtualizador;

end;

destructor TAtualizador.Destroy;
begin

  inherited;
end;

procedure TAtualizador.GetArquivoIniFTP;
var
  localSalvar: string;
  resource: TResourceStream;
  arquivo: TFileStream;
begin
  localSalvar := ExtractFilePath(ParamStr(0)) + 'att.ini';

  if not FileExists(localSalvar) then
  begin
    resource := TResourceStream.Create(HInstance, 'att', RT_RCDATA);
    arquivo := TFileStream.Create(localSalvar, fmCreate);
    try
      resource.SaveToStream(arquivo);
    finally
      resource.Free;
      arquivo.Free;
    end;
  end;

end;

procedure TAtualizador.GetAtualizador;
var
  localSalvar: string;
  resource: TResourceStream;
  arquivo: TFileStream;
begin
  localSalvar := ExtractFilePath(ParamStr(0)) + 'att.exe';

  if FileExists(localSalvar) then
    DeleteFile(PWideChar(localSalvar));

  resource := TResourceStream.Create(HInstance, 'atualizador', RT_RCDATA);
  arquivo := TFileStream.Create(localSalvar, fmCreate);
  try
    resource.SaveToStream(arquivo);
  finally
    resource.Free;
    arquivo.Free;
  end;
end;

function TAtualizador.GetVersaoFTP: integer;
var
  strVersao: string;
  arquivoINI: TIniFile;
  nomePrograma, localPrograma, nomeArquivoINI, localIni: string;
  FTP: TFTPConfig;
begin
  Result:= 0;
  nomePrograma:= ExtractFileName(ParamStr(0));
  nomePrograma:= ChangeFileExt(nomePrograma, '');
  localPrograma:= ExtractFilePath(ParamStr(0));
  nomeArquivoINI:='versaoFTP.ini';
  localIni:= localPrograma + nomeArquivoINI;
  if FileExists(nomeArquivoINI) then
    DeleteFile(PWideChar(nomeArquivoINI));

  FTP:= TFTPConfig.Create(FUser, FPass, FServ, FPort);
  try
    if Assigned(FTP) then
    begin
      FTP.Conectar;
      FTP.Baixar(nomeArquivoINI, localPrograma);

      if FileExists(LocalPrograma+nomeArquivoINI) then
      begin
        arquivoINI:= TIniFile.Create(LocalPrograma+nomeArquivoINI);
        try
          strVersao:= arquivoINI.ReadString('versao', nomePrograma, EmptyStr);

          strVersao:= StringReplace(strVersao, '.', EmptyStr, [rfReplaceAll]);

          Result:= StrToIntDef(strVersao, 0);
        finally
          arquivoINI.Free;
        end;
      end;
    end;
  finally
    FTP.Free;
  end;
end;

procedure TAtualizador.limparArquivo;
var
  atualizador: string;
  arquivoINi: string;
  arquivoFTP:string;
  arquivoZip:string;
  local: string;
  exe: string;
begin
  exe:= ExtractFileName(ParamStr(0));
  exe:= ChangeFileExt(exe, '');
  local:= ExtractFilePath(ParamStr(0));
  arquivoFTP:= local+ 'versaoFTP.ini';
  arquivoZip:= local+ exe + '.zip';
  arquivoINi:= local + 'att.ini';
  atualizador:= local+ 'att.exe';

  if FileExists(arquivoFTP) then
    DeleteFile(PWideChar(arquivoFTP));

  if FileExists(arquivoZip) then
    DeleteFile(PWideChar(arquivoZip));

  if FileExists(arquivoINi) then
    DeleteFile(PWideChar(arquivoINi));

  if FileExists(atualizador) then
    DeleteFile(PWideChar(atualizador));
end;

initialization
  try
    if temInternet then
      atualiazador:= TAtualizador.Create();
  except
  end;


finalization

  if Assigned(atualiazador) then
    atualiazador.Free;

end.
