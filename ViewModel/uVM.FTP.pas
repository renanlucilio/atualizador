unit uVM.FTP;

interface

uses
  {Delphi}
    VCL.Forms,
    System.SysUtils,
    System.Classes,
    System.Zip,
  {Indy}
    IdComponent,
  {FTP}
    uFTPConfig,
  {Model}
    uProgramaAtualizavel,
  {Tratamento Arquivo}
    uTratamentoArquivoZip;

type
  TdmFTP = class(TDataModule)
    procedure DataModuleDestroy(Sender: TObject);
  private
    FProgramaSerAtualizado: TProgramaAtualizavel;
    FFTP: TFTPConfig;
    procedure DoBaixando(ASender: TObject; AWorkMode: TWorkMode;AWorkCount: Int64);
    procedure DoComecoBaixar(ASender: TObject; AWorkMode: TWorkMode; AWorkCountMax: Int64);
  public
    property FTP:                   TFTPConfig               read FFTP                   write FFTP;
    property ProgramaSerAtualizado: TProgramaAtualizavel     read FProgramaSerAtualizado write FProgramaSerAtualizado;

    procedure iniciar;
  end;

var
  dmFTP: TdmFTP;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses
  uViewPrincipal, Winapi.Windows;

{$R *.dfm}

{ TdmFTP }

procedure TdmFTP.DataModuleDestroy(Sender: TObject);
begin
  if Assigned(FFTP) then
    FFTP.Free;

  if Assigned(FProgramaSerAtualizado) then
    FProgramaSerAtualizado.Free;

end;

procedure TdmFTP.DoBaixando(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
var
  tamanhoTotal, tamanhoTransmitido, progresso: double;
begin
  if (FFTP as TFTPConfig).TamanhoDoArquivo = 0 then
    exit;

  Application.ProcessMessages;

  tamanhoTotal:= (FFTP as TFTPConfig).TamanhoDoArquivo div 1024;

  tamanhoTransmitido:= AWorkCount div 1024;

  progresso:= (tamanhoTransmitido*100)/tamanhoTotal;

  frmPrincipal.lblPorcentagem.Caption := Format('%s%%', [FormatFloat('##0', progresso)]);
  frmPrincipal.pgbProgresso.Position := AWorkCount div 1024;
end;

procedure TdmFTP.DoComecoBaixar(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
begin
  frmPrincipal.lblProcesso.Caption:= 'Baixando atualiza��o: ';
  frmPrincipal.pgbProgresso.Max := (FFTP as TFTPConfig).TamanhoDoArquivo div 1024;
end;


procedure TdmFTP.iniciar;
begin
 try
    if not FileExists(ExtractFilePath(ParamStr(0))+'att.ini') then
      raise Exception.Create('Arquivo de conex�o foi encontrado');

    FFTP:= TFTPConfig.Create;
    FProgramaSerAtualizado:= TProgramaAtualizavel.Create(FFTP);
    try
      frmPrincipal.Label1.Caption:= 'Atualizando: '+ FProgramaSerAtualizado.NomePrograma;

      FTP.OnBaixando:= DoBaixando;
      FTP.OnComecoBaixar:= DoComecoBaixar;
      ProgramaSerAtualizado.Atualizar;

      Application.Terminate;
    finally
      FProgramaSerAtualizado.Free;
    end;
 except
    on E: Exception do
    begin
      Application.MessageBox('Houve um erro a executar a atualiza��o', 'Erro', MB_OK+MB_ICONERROR);
      Application.Terminate;
    end;
  end;
end;



end.
